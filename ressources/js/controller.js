(function() {

var cahApp = angular.module('cahApp', []);

cahApp.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

cahApp.controller('NavbarLeftListCtrl', function ($scope, $http) {
    $http.get('json/navbar_left.json')
		.success(function (data) {
		     $scope.pages = data;
		 })
		.error(function (data, status, headers, config) {
		 //  Do some error handling here
		});
});
cahApp.controller('NavbarRightListCtrl', function ($scope, $http) {
    $http.get('json/navbar_right.json')
		.success(function (data) {
		     $scope.pages_right = data;
		 })
		.error(function (data, status, headers, config) {
		 //  Do some error handling here
		});
});

cahApp.controller('PresentationListCtrl', function ($scope, $http) {
    $http.get('json/presentation.json')
		 .success(function (data) {
		     $scope.instructions = data;
		 })
		.error(function (data, status, headers, config) {
		 //  Do some error handling here
		});
});
cahApp.controller('TeamListCtrl', function ($scope, $http) {
    $http.get('json/team.json')
		 .success(function (data) {
		     $scope.instructions = data;
		 })
		.error(function (data, status, headers, config) {
		 //  Do some error handling here
		});
});
cahApp.controller('DownloadListCtrl', function ($scope, $http) {
    $http.get('json/download.json')
		 .success(function (data) {
		     $scope.instructions = data;
		 })
		.error(function (data, status, headers, config) {
		 //  Do some error handling here
		});
});

cahApp.controller('PanelController', function (){
	this.tab = 'present';

	this.selectTab = function(setTab) {
		this.tab= setTab;
	};
	this.isSelected = function(checkTab) {
		return this.tab === checkTab;
	};
	this.isNull = function(checkString) {
		if (checkString == "") {
			return true;
		}
		else {
			return false;
		}
	};
});

})();